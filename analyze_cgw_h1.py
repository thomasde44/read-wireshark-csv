import pandas
import re
import matplotlib.pyplot as plt
import sys


h_data = pandas.read_csv(sys.argv[1])

cgw = []
pattern = re.compile("Win=")
for i in h_data['Info']:
    # print(i)

    tmp = re.split(" ", i)
    for i in tmp:
        if pattern.match(i):
            num = re.findall('[0-9]+', i)
            cgw.append(int(num[0]))

final_data = pandas.DataFrame(cgw)

print(final_data.describe())

ts = pandas.Series(cgw)
plt.xlabel("packets")
plt.ylabel("window size")
ts.plot()
# ts.plot()    
plt.show()      
